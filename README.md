# Ansible role for Ntpd installation

## Introduction

[Ntpd](https://www.ntp.org/) is a complete NTP server implementation,
running as a daemon.

This role installs and configure the server. Setting up the firewall is not done yet (but planned).

## Prerequisite

In order to properly rate-limit per-source, and avoid blocking all clients if one is misbehaving, kernel options have to be setup.
It is not done in this role because you might wish to adjust the settings differently depending on other needs.

The ip_pkt_list_tot setting of the xt_recent module needs to be at least 100.

You could create a file named '/etc/modprobe.d/firewall_kmod.conf' containing:

    options xt_recent ip_pkt_list_tot=100

## Variables

- **servers**: list of servers to synchronize with
- **servers_preferred**: optional additional servers to synchronize with; these ones would be preferred over the `servers` list if they work well
- **access_policy**: optional information about the access policy (who is allowed to access the server and rules)
- **info_url**: optional URL to provide more information about who runs the NTP server
- **admin_contact**: optional email contact to reach the NTP server administrators
- **whitelist**: list of CIDRs (use /32 for single Ips) allowed to access time on this server (default is to allow anyone)
- **with_stats**: generate statistics if True

## Firewalling configuration

By default the role opens the needed ports using Firewalld. You can disable this by setting `manage_firewall` to False.

