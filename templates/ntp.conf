# {{ ansible_managed }}
# {{ ntp_conffile }}, configuration for ntpd; see ntp.conf(5) for help

driftfile {{ ntp_driftfile }}

{% for server in servers_preferred %}
{% if server is search('pool') %}
pool {{ server }} iburst prefer
{% else %}
server {{ server }} iburst prefer
{% endif %}
{% endfor %}
{% for server in servers %}
{% if server is search('pool') %}
pool {{ server }} iburst
{% else %}
server {{ server }} iburst
{% endif %}
{% endfor %}

# /!\ beware of the spaces around "=", they are not cosmetic...
{% if access_policy is defined %}
setvar access_policy = "{{ access_policy }}" default
{% endif %}
{% if admin_contact is defined %}
setvar admin_contact = "{{ admin_contact }}" default
{% endif %}
{% if info_url is defined %}
setvar info_url = "{{ info_url }}" default
{% endif %}

# By default, exchange time with everybody, but don't allow configuration.
restrict -4 default kod notrap nomodify nopeer #noquery
restrict -6 default kod notrap nomodify nopeer #noquery

# Local users may interrogate the ntp server more closely.
restrict 127.0.0.1
restrict ::1

# mitigate http://support.ntp.org/bin/view/Main/SecurityNotice#DRDoS_Amplification_Attack_using
disable monitor

{% if with_stats %}
# statistics
statsdir {{ ntp_stats_dir }}
statistics loopstats peerstats clockstats
filegen loopstats file loopstats type day enable
filegen peerstats file peerstats type day enable
filegen clockstats file clockstats type day enable
{% endif %}

